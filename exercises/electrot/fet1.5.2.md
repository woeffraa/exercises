1. Que se passe-t-il lorsqu'un courant traverse une résis-tance? 
2. Quelle est l'énergie dégagée par une résistance de 34 O parcourue par un courant de 6,76 A pendant 65 minutes? 
3. Qu'est-ce que la chaleur massique? 
4. Quels sont les symboles de grandeur et d'unité de la différence de température? 
5. Calculer l'énergie nécessaire pour chauffer 300 litres d'eau de 12°C à 85°C. 
6. De quoi dépend l'énergie calorifique utile? 
7. On dispose d'une bouilloire de 1 kW pour élever la tem-pérature d'un litre d'eau de 20°C à 100°C. Le rendement est de 92%. Combien de minutes seront nécessaires pour chauffer cette eau? 
8. Calculer la puissance d'une friteuse devant élever de 60°C la température de 2 litres d'huile (masse volu-mique p = 0,91 kg/dm3). Le rendement est de 90% et la durée de chauffe ne doit pas dépasser 4 minutes. 
9. Quelle est l'énergie perdue pendant 3 heures dans une ligne en cuivre de 20 m de longueur et de 1,5 mm2 de section si le courant est de 9,2 A? 
10. Qu'est-ce que la densité de courant? 
11. Quels sont les symboles de grandeur et d'unité de la densité de courant? 
12. Calculer la densité de courant dans une ligne en cuivre de 150 m de longueur, réalisée en fil de 3,5 mm de diamètre, et parcourue par un courant de 40 A. 
13. Qu'est-ce que le coefficient de température? 
14. Quels sont les symboles de grandeur et d'unité du coefficient de température? 
15. Comment se comporte la résistance d'un conducteur en cuivre que l'on chauffe ? 
16. Quels sont les matériaux dont la résistance diminue lorsque la température augmente? 
17. La résistance d'une bobine de fil de cuivre est de 4,6 0 à 20°C. Quelle sera sa résistance lorsque la bobine aura atteint une température de 55°C? 
18. Le filament d'une lampe à incandescence a une résis-tance de 35,5 Q à 20 °C. En service normal, cette lampe est parcourue par un courant de 450 mA sous une tension de 230 V. Calculer la température du fila-ment en fonctionnement. 
19. Un appareil thermique dissipe une puissance de 1200 W sous 230 V. Son corps de chauffe est constitué d'un fil de chrome-nickel de section 0,48 mm2 et atteint en service 640°C. Calculer: 
a) la longueur du fil utilisé; b) la nouvelle puissance si on a enlevé 15% de la longueur du fil et que la tension a chuté à 215 V. 
20. Une résistance a une valeur de 12,4 0 à 85°C. Calculer sa valeur à 210°C, sachant que son coefficient de température est de 0,0035 à 20°C. 
21. Une résistance a une valeur de 12,4 0 à 5°C. Calculer sa valeur à 210°C, sachant que son coefficient de température est de 0,0035 à 20°C. 
22. Une résistance a une valeur de 12,4 0 à -5°C. Calculer sa valeur à 210°C, sachant que son coefficient de température est de 0,0035 à 20°C. 

9. Une pile de FEM 1,5 V débite un courant de 0,25 A dans une résistance de 5 0. Calculer dans ce cas la tension aux bornes de la pile et la résistance interne de la pile. 
10. Une pile de type LR6 de 1,5 V a une capacité de 2,5 Ah pour un prix de vente d'environ Fr. 1,00. Calculer le prix du kWh de l'énergie fournie par la pile. De combien cette énergie est-elle plus chère que l'énergie fournie par le réseau, si celle-ci coûte 11 cts le kWh ? 
11. Une pile a une FEM Ede 1,5 V. Elle est mise en court-circuit par un ampèremètre de résistance interne négli-geable, qui indique 12 A. Quelle est la résistance interne de la pile et la tension au moment du court-circuit? 
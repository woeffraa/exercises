---
points: 5
---

Un appareil thermique dissipe une puissance de $$1200\ W$$ sous $$230\ V$$. 
Son corps de chauffe est constitué d'un fil de chrome-nickel de section $$0.48\ mm^{2}$$ et atteint en service 
$$640\ ^{\circ}C$$. 

Calculer: 

1. la longueur du fil utilisé. 
2. la nouvelle puissance si on a enlevé $$15\%$$ de la longueur du fil et que la tension a chuté à $$215\ V$$. (On considère que la température du corp de chauffe reste à $$640\ ^{\circ}C$$.)

---
points: 2
time: 5
---

Une résistance a une valeur de $$12.4\ \Omega$$ à $$-5\ ^{\circ}C$$. Calculer sa valeur à $$210\ ^{\circ}C$$, 
sachant que son coefficient de température est de $$0.0035$$ à $$20\ ^{\circ}C$$. 

---
points: 2
time: 6
---

Calculer la densité de courant dans une ligne en cuivre de $$150\ m$$ de longueur, réalisée en fil de $$3.5\ mm$$ 
de diamètre, et parcourue par un courant de $$40\ A$$.


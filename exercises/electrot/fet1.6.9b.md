---
points: 4
time: 6
---

Une pile de FEM $$1.5\ V$$ et de capacité $$2\ Ah$$ débite un courant de $$0.25\ A$$ dans une résistance de $$5\ \Omega$$. 

Calculer\ :
 
1. la résistance interne de la pile.
2. la tension aux bornes de la pile.
3. la durée avant que la pile ne soit vide.
4. la puissance dissipée par la pile.

---
points: 3
time: 6
---

Une pile a FEM de $$1.5\ V$$. Elle est mise en court-circuit par un ampèremètre de résistance 
interne négligeable, qui indique $$10\ A$$.

Déterminer :

1. la résistance interne de la pile.
2. la tension aux bornes de la pile.

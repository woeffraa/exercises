---
points: 2
time: 6
---

Une pile de FEM $$1.5\ V$$ débite un courant de $$0.25\ A$$ dans une résistance de $$5\ \Omega$$. 
Calculer dans ce cas la tension aux bornes de la pile et la résistance interne de la pile. 

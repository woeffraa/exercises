---
papersize: A4
fontsize: 11pt
margin-top: 0cm
margin-right: 1.5cm
margin-bottom: 1.5cm
margin-left: 2.5cm
fontfamily: charter 
---

\def\arraystretch{1.3}
\begin{tabular}{|L{.15\textwidth}|C{.30\textwidth}|L{.2\textwidth}|R{.2\textwidth}|C{.05\textwidth}|}
\hline 
Date & $date & Classe & \multicolumn{2}{c|}{ $class } \\ 
\hline 
Nom &  & Prénom & \multicolumn{2}{c|}{} \\ 
\hline 
\multicolumn{3}{|c|}{\Large{ \textbf{ $title }}} & {\textbf{Note} :} &  \\ 
\hline 
\multicolumn{3}{|c|}{\textbf{ $subtitle }} & pts : &  \\ 
\hline 
\multicolumn{2}{|l|}{ $index } & Durée : $duration & Total pts : & $points \\ 
\hline 
\multicolumn{5}{|L{\textwidth}|}{\small{ $notice }}\\
\hline 
\end{tabular} 




#!/usr/lib/python

from configparser import ConfigParser
from getopt import GetoptError, getopt
import sys
import os
import subprocess
import yaml
import re

PANDOC_EXE = "pandoc"
# PANDOC_EXE = "C:\\Users\\alain\\AppData\\Local\\Pandoc\\pandoc.exe"

duration = False
reference = False

def main(argv):

    global duration, reference

    # parse script arguments
    try:
        opts, args = getopt(argv,"hvdr",[ "help", "version", "duration", "reference"])
    except GetoptError:
        showHelp()
        sys.exit(2)

    for opt, arg in opts :
        if opt in ('-h', '--help'):
            showHelp()
            sys.exit()
        if opt in ('-v', '--version'):
            showVersion()
            sys.exit()
        if opt in ('-d', '--duration'):
            duration = True
        if opt in ('-r', '--reference'):
            reference = True

    mdFile = argv[0].replace('.yml', '.md')

    try:
        os.remove(mdFile)
    except Exception:
        pass

    prepare(argv[0])
    compile(mdFile)

def prepare(file):

    with open(file) as configFile:
        configText = configFile.read()
        config = yaml.load(configText)
        mdFile = file.replace('.yml','.md')

        with open(mdFile, "a") as mainFile:
            mainRaw = ""
            with open("./src/header.md", "r") as header:
                mainRaw = header.read()
                header.close()

            index = 1
            totalPts = 0

            for exercise in config['exercises']:
                with open("./exercises/%s.md" % exercise, "r") as exerciseFile:
                    raw = exerciseFile.read()
                    regex = r"---(.*)---"

                    matches = re.search(regex, raw, re.DOTALL)

                    if matches:
                        exerciceConfig = yaml.load(matches.groups()[0])
                        raw = raw.replace("---" + matches.groups()[0] + "---\n", "")

                    print(exerciceConfig)
                    totalPts += int(exerciceConfig['points'])
                    # mainRaw += "\\begin{samepage}\n"
                    mainRaw += ("# Exercice %d (%dpts)\n\n" % (index, exerciceConfig['points']))

                    if reference:
                        mainRaw += "Ref. : %s\n" % exercise

                    if duration:
                        try:
                            mainRaw += ("Durée : %s min\n" % exerciceConfig['duration'])
                        except KeyError:
                            mainRaw += ("Durée : - \n")

                    mainRaw += (raw)
                    mainRaw += ("\n")
                    # mainRaw += "\\end{samepage}\n"
                    index = index+1
                    exerciseFile.close()

            regex = r"\s\$(\w+)\s"
            matches = re.finditer(regex, mainRaw)

            config['points'] = str(totalPts)

            print(config)

            for match in matches:
                var = match.groups()[0]
                try:
                    mainRaw = mainRaw.replace("$%s" % var, config[var])
                except Exception:
                    pass

            mainRaw = mainRaw.replace("$$", "$")

            mainFile.write(mainRaw)

            mainFile.close()
        configFile.close()



def compile(mdFile):
    pdfFile = mdFile.replace('.md', '.pdf')
    subprocess.call(PANDOC_EXE + " %s -t latex --include-in-header=./src/header.tex -o %s" % (mdFile, pdfFile),shell=True)

def checkPandoc():

    subprocess.call(PANDOC_EXE + " --version", shell=True)

def showVersion():
    # load project config
    path = os.path.dirname(os.path.realpath(__file__))
    project = ConfigParser()
    project.read(path + '/../project.cfg')

    # print version
    print("\n%s v%s (%s)" % (
        project.get('general', 'name'),
        project.get('general', 'version'),
        project.get('general', 'date')
    ))

def showHelp():
    print( "\nUsage : docgen [option]") # + " [-c file]"
    print( "\nOptions :" )
    print( "-h, --help       : Print the current help and exit.")
    print( "-v, --version    : Print the version information and exit.")
    print( "--duration       : Show the duration of exercices.")

if __name__ == "__main__":

    main(sys.argv[1:])

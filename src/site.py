#!/usr/lib/python3
import os
import re
from hgext.zeroconf import config

import yaml
import subprocess

def getConfig(filename):
    with open(filename, "r") as exerciseFile:
        conf = {}
        raw = exerciseFile.read()
        regex = r"---(.*)---"

        matches = re.search(regex, raw, re.DOTALL)

        if matches:
            conf = yaml.load(matches.groups()[0])

    return conf


def html(filename):
    output = ""
    # filename = "./exercises/%s.md" % exercise
    conf = getConfig(filename)

    proc = subprocess.Popen("pandoc %s" % filename,stdout=subprocess.PIPE, shell=True)

    for line in iter(proc.stdout.readline, ''):
        output += line.rstrip()

    return output, conf


branches = os.listdir('./exercises')

with open("./site/index.html", "w") as indexFile:
    indexFile.write('<html>\n')
    indexFile.write('<ul>\n')

    for branch in branches :
        indexFile.write('<li>\n')
        indexFile.write('<a href="%s.html">%s</a>\n' % (branch, branch) )
        indexFile.write('</li>\n')

        exercises = os.listdir('./exercises/' + branch)
        with open("./site/%s.html" % branch, "w") as branchFile:

            for exercise in exercises:
                if exercise.endswith(".md"):
                    raw, conf = html('./exercises/'+ branch + '/' + exercise)

                    title = exercise.replace(".md", "")

                    branchFile.write('<div class="exercise" id="%s">' % title)
                    branchFile.write('<h1>%s</h1>' % title)
                    branchFile.write(raw)
                    branchFile.write('</div>')
                elif exercise.endswith(".png"):

                    pass

            branchFile.close()

    indexFile.write('</ul>\n')
    indexFile.write('</html>\n')

    indexFile.close()
